package com.ctcorp.digital.dto.response;

public class Result {

    private static final String SUCCESS = "success";
    private static final String FAILED = "failed";

    private boolean success;
    private String message;
    private Object data;


    public Result() {
    }

    public Result(boolean success, String message, Object data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public Result(Object data) {
        this.success = true;
        this.message = SUCCESS;
        this.data = data;
    }

    public Result(boolean isSuccess) {
        this.success = isSuccess;
        this.message = getMessage(isSuccess);
      }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage(boolean isSuccess) {
        return (isSuccess) ? SUCCESS : FAILED;
    }
}
