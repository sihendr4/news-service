package com.ctcorp.digital.dto.request;

import com.ctcorp.digital.model.Tag;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class PostRequest {

    @NotNull(message = "title is mandatory")
    private String title;

    @NotNull(message = "content is mandatory")
    @Size(min = 10, message = "content is must grater than 10 character")
    private String content;

    private List<Long> tags;

    public PostRequest(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public PostRequest(String title, String content, List<Long> tags) {
        this.title = title;
        this.content = content;
        this.tags = tags;
    }

    public PostRequest() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Long> getTags() {
        return tags;
    }

    public void setTags(List<Long> tags) {
        this.tags = tags;
    }
}
