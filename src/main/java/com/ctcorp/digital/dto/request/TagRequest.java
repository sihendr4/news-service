package com.ctcorp.digital.dto.request;


import javax.validation.constraints.NotNull;

public class TagRequest {

    @NotNull(message = "label is mandatory")
    private String label;

    public TagRequest() {
    }

    public TagRequest(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
