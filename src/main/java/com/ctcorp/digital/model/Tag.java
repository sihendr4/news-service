package com.ctcorp.digital.model;


import com.ctcorp.digital.dto.request.TagRequest;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


@Entity
public class Tag {

    @Id
    @GeneratedValue
    private Long id;
    private String label;

    @ManyToMany(mappedBy = "tags")
    @JsonBackReference
    private Set<Post> posts = new HashSet<>();

    public void addPost(Post post){
        posts.add(post);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    public Tag(Long id, String label, Set<Post> posts) {
        this.id = id;
        this.label = label;
        this.posts = posts;
    }

    public Tag() {
    }

    public Tag(TagRequest tagRequest) {
        this.label = tagRequest.getLabel();
    }

    public Tag(String label) {
        this.label = label;
    }


}
