package com.ctcorp.digital.repository;

import com.ctcorp.digital.model.Tag;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class TagRepository implements PanacheRepository<Tag> {

    public Optional<Tag> findByLabel(String label){
        return Optional.ofNullable(find("label", label).firstResult());
    }
}
