package com.ctcorp.digital.repository;

import com.ctcorp.digital.model.Post;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class PostRepository implements PanacheRepository<Post> {
 }
