package com.ctcorp.digital.controller;

import com.ctcorp.digital.dto.request.PostRequest;
import com.ctcorp.digital.dto.response.Result;
import com.ctcorp.digital.model.Post;
import com.ctcorp.digital.model.Tag;
import com.ctcorp.digital.repository.PostRepository;
import com.ctcorp.digital.repository.TagRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("/api/v1/post")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostController {

    @Inject
    PostRepository postRepository;

    @Inject
    TagRepository tagRepository;

    @GET
    public Response getAll(){
        return Response.ok(new Result(postRepository.findAll().list())).build();
    }

    @POST
    @Transactional
     public Response create(@Valid PostRequest postRequest){
        Post post = new Post(postRequest);
        postRequest.getTags().forEach(idTag -> tagRepository.findByIdOptional(idTag).ifPresent(post::addTag));
        postRepository.persist(post);

        return Response.ok(new Result(true)).build();
    }

    @Path("/{id}")
    @PUT
    @Transactional
    public Response update(@PathParam("id") Long id, @Valid PostRequest postRequest){
        Post post = postRepository.findById(id);
        if(post == null)
            throw new NotFoundException("Post with id of " + id + " does not exist.");

        postRepository.update("title = ?1, content = ?2 where id = ?3",
                postRequest.getTitle(),
                postRequest.getContent(),
                id);
        post.getTags().forEach(post::removeTag);

        postRequest.getTags().forEach(idTag -> tagRepository.findByIdOptional(idTag).ifPresent(post::addTag));


//        for(String label : postRequest.getTags()){
//            if(tagRepository.findByLabel(label).isPresent()){
//                post.addTag(tagRepository.findByLabel(label).get());
//            }else{
//                post.addTagByString(label);
//            }
//        }

        return Response.ok(new Result(true)).build();
    }

    @Path("/{id}")
    @DELETE
    @Transactional
    public Response delete(@PathParam("id") Long id){
        Post post = postRepository.findById(id);
        if(post == null)
            throw new NotFoundException("Post with id of " + id + " does not exist.");

        post.getTags().forEach(tag -> tag.getPosts().remove(tag));
        postRepository.delete(post);

        return Response.ok(new Result(true)).build();
    }
}
