package com.ctcorp.digital.controller;

import com.ctcorp.digital.dto.request.TagRequest;
import com.ctcorp.digital.dto.response.Result;
import com.ctcorp.digital.model.Tag;
import com.ctcorp.digital.repository.TagRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/v1/tag")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TagController {

    @Inject
    TagRepository tagRepository;

    @GET
    public Response getAll(){
        return Response.ok(new Result(tagRepository.findAll().list())).build();
    }

    @POST
    @Transactional
     public Response create(@Valid TagRequest tagRequest){
        Tag tag = new Tag(tagRequest);
        tagRepository.persist(tag);

        return Response.ok(new Result(true)).build();
    }

    @Path("/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Response update(@PathParam("id") Long id, @Valid TagRequest tagRequest){
        Tag post = tagRepository.findById(id);
        if(post == null)
            throw new NotFoundException("Tag with id of " + id + " does not exist.");

        tagRepository.update("label = ?1 where id = ?2",
                tagRequest.getLabel(),
                id);

        return Response.ok(new Result(true)).build();
    }

    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Response delete(@PathParam("id") Long id){
        Tag tag = tagRepository.findById(id);
        if(tag == null)
            throw new NotFoundException("Tag with id of " + id + " does not exist.");

        tagRepository.delete(tag);

        return Response.ok(new Result(true)).build();
    }
}
