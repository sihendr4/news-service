# news-service project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Stack
 1.  Java - JDK 1.8
 2.  Database - PostgreSQL 13. or latest version
 3.  Postman - include in this project, in folder *postman*
 
